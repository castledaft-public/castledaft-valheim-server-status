const React = require('react')
const ReactDOM = require('react-dom')
const axios = require('axios')

class Overview extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            hostname: '',
            password: '',
            version: '',
            day: 0
        }

        this.interval = false;
    }

    componentDidMount() {
        let self = this;
        this.getOverview.bind(this)();

        this.interval = setInterval(() => {
            self.getOverview();
        }, 5000);
    }

    getOverview() {
        let self = this;
        axios.get('/api/overview').then((response) => {
            self.setState({
                hostname: response.data.hostname,
                password: response.data.password,
                version: response.data.serverVersion,
                day: response.data.serverDay
            });
        });
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <div className="card bg-orange-400 border-orange-400 shadow-md text-gray-800 mb-auto">
                <div className="card-body flex flex-row md:flex-col items-center p-4 px-6">

                    <div className="md:mx-auto img-wrapper w-72 md:w-48 flex justify-center items-center py-6">
                        <img src="./img/beer.svg" alt="img title" />
                    </div>

                    <div className="py-2 ml-10 md:ml-0 w-full">
                        <h1 className="h6">Valheim</h1>
                        <ul className="mt-4 w-100 flex flex-col">
                            <li className="text-sm font-light flex flex-row md:flex-col md:mb-3">
                                <span className="inline-block w-1/3">Address:</span>
                                <code>{this.state.hostname}</code>
                            </li>
                            <li className="text-sm font-light flex flex-row md:flex-col md:mb-3">
                                <span className="inline-block w-1/3">Password:</span>
                                <code className="inline-block bg-gray-800 text-gray-800 hover:bg-gray-200 md:bg-gray-200 rounded px-1">{this.state.password}</code>
                            </li>
                            <li className="text-sm font-light flex flex-row md:flex-col">
                                <span className="inline-block w-1/3">Version:</span>
                                <code>{this.state.version}</code>
                            </li>
                            <li className="text-sm font-light flex flex-row md:flex-col">
                                <span className="inline-block w-1/3">Day:</span>
                                <code>{this.state.day}</code>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        );
    }
}

class Player extends React.Component {
    render() {
        let wrapperClasses = "hover:opacity-100 duration-200 transition-opacity p-6 flex flex-col text-gray-600 border-b";
        let characterNameClasses = "";
        let steamIdClasses = "text-sm";
        let playTimeClasses = "flex items-center";

        let isOffline = this.props.player.isOffline;
        let characterName = this.props.player.lastCharacter ? this.props.player.lastCharacter.characterName : 'Unknown';
        let steamId = this.props.player.steamId;
        let infoBlobClasses = "mr-4";
        let currentOrLastPlayTimeString = "ml-auto"

        if (isOffline) {
            wrapperClasses += " opacity-75 bg-gray-100";
        }

        if (this.props.lights !== true) {
            if (isOffline) {
                wrapperClasses += " bg-gray-800"
            } else {
                wrapperClasses += " bg-gray-700"
            }

            wrapperClasses += " text-gray-100 border-gray-900"
            characterNameClasses += "text-gray-200";
            steamIdClasses += " text-gray-500";
            playTimeClasses += " text-gray-200";
        }

        return (
            <div className={wrapperClasses}>
                <div className="flex flex-row justify-between items-center">
                    <div className="flex flex-col">
                        <h2 className={characterNameClasses}>{characterName}</h2>
                        <span className={steamIdClasses}>{steamId}</span>
                    </div>
                    <div
                        className={playTimeClasses}>
                        {this.props.player.playTimeString || ''}
                    </div>
                </div>
                <span className="mt-3 text-gray-500 flex flex-row">
                    <span className={infoBlobClasses} title="Player Deaths">
                        <i className="fa fa-skull text-gray-400"/> {this.props.player.deaths || ''}
                    </span>
                    <span className={infoBlobClasses} title="Characters">
                        <i className="fa fa-user text-gray-400"/> {this.props.player.characters || ''}
                    </span>
                    <span className={infoBlobClasses} title="Sign Ins">
                        <i className="fa fa-sign-in-alt text-gray-400"/> {this.props.player.sessions || ''}
                    </span>
                    <span className={currentOrLastPlayTimeString}>{!isOffline ? 'Current Session' : 'Last Online'}: {!isOffline ? this.props.player.currentPlayTimeString || '' : this.props.player.lastOnlineString || ''}</span>
                </span>
            </div>
        );
    }
}

class PlayerList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            players: [],
            onlinePlayers: 0
        };

        this.interval = false;
    }

    componentDidMount() {
        let self = this
        this.getPlayers.bind(this)()

        this.interval = setInterval(() => {
            self.getPlayers.bind(self)()
        }, 5000);
    }

    getPlayers() {
        let self = this
        axios.get('/api/players').then((response) => {
            self.setState({
                players: response.data.players || [],
                onlinePlayers: response.data.onlinePlayers || 0
            });
        });
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    render() {
        let cardClasses = "card"
        let headerClasses = "card-header"
        if (this.props.lights !== true) {
            cardClasses += " bg-gray-900 text-gray-100 border-gray-900"
            headerClasses += " border-gray-900"
        }

        return (
            <div className={cardClasses}>
                <div className={headerClasses}>
                    Players
                    <span className="lowercase">({this.state.onlinePlayers} / {this.state.players.length} online)</span>
                </div>
                {this.renderPlayers()}
            </div>
        );
    }

    renderPlayers() {
        let players = [];

        for (let player of this.state.players) {
            players.push(<Player key={player.id} lights={this.props.lights} player={player} template={false} />);
        }

        return players;
    }
}

class LightSwitch extends React.Component {
    render() {
        let wrapperClasses = "card shadow-md mb-4 mt-auto"
        if (this.props.lights !== true) {
            wrapperClasses += " bg-gray-900 border-gray-900 text-gray-100";
        }

        return (
            <div
                className={wrapperClasses}>
                <div className="card-body">
                    <button type="submit" name="toggle-lights" id="toggle-lights" value="1" onClick={this.props.onToggleClick.bind(this)}>
                        <i className="fa fa-sun mr-3" />
                        Turn {this.props.lights === true ? 'off' : 'on'} the lights
                    </button>
                </div>
            </div>
        );
    }
}

class Footer extends React.Component {
    render() {
        let wrapperClasses = "p-2 pb-4 text-center duration-200 transition-colors";
        let aClasses = "";

        if (this.props.lights === true) {
            wrapperClasses += " text-gray-400"
            aClasses = "hover:text-gray-600"
        } else {
            wrapperClasses += "text-gray-600"
            aClasses = "hover:text-gray-400"
        }

        return (
            <div className={wrapperClasses}>
                <p>Created by <a
                    className={aClasses}
                    href="https://gitlab.com/castledaft-public" target="gitlab">Jack Stupple (SurDaft)</a> - Adaptation of Cleopatra Tailwind theme.</p>
            </div>
        );
    }
}

class Grid extends React.Component {
    constructor(props) {
        super(props);

        let lights = localStorage.getItem('lights') || 'on';
        if (lights === 'off') {
            document.body.className = "bg-gray-800";
        }

        this.state = {
            lights:  lights === 'on'
        }
    }

    onToggleClick(event) {
        event.preventDefault();

        let lights = !this.state.lights;

        document.body.className = lights ? 'bg-gray-100' : 'bg-gray-800';
        localStorage.setItem('lights', lights ? 'on' : 'off')
        this.setState({lights: lights})
    }

    render() {
        return (
            <div className={"mt-2 grid grid-cols-1 gap-6"}>
                <Overview lights={this.state.lights} />
                <PlayerList lights={this.state.lights} />
                <LightSwitch lights={this.state.lights} onToggleClick={this.onToggleClick.bind(this)} />
                <Footer lights={this.state.lights} />
            </div>
        );
    }
}

ReactDOM.render(<Grid />, document.getElementById('root'))
