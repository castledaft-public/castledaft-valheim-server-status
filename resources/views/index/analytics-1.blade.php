<div class="mt-6 grid grid-cols-1 gap-6">

    <!-- update section -->
    <div class="card bg-orange-400 border-orange-400 shadow-md text-gray-800 mb-auto">
        <div class="card-body flex flex-row md:flex-col items-center p-4 px-6">

            <!-- image -->
            <div class="md:mx-auto img-wrapper w-72 md:w-48 flex justify-center items-center py-6">
                <img src="./img/beer.svg" alt="img title">
            </div>
            <!-- end image -->

            <!-- info -->
            <div class="py-2 ml-10 md:ml-0 w-full">
                <h1 class="h6">Valheim</h1>
                <ul class="mt-4 w-100 flex flex-col">
                    <li class="text-sm font-light flex flex-row md:flex-col md:mb-3"><span class="inline-block w-1/3">Address:</span><code>{{ $hostname }}</code></li>
                    <li class="text-sm font-light flex flex-row md:flex-col md:mb-3"><span class="inline-block w-1/3">Password:</span><code class="inline-block bg-gray-800 text-gray-800 hover:bg-gray-200 md:bg-gray-200 rounded px-1">{{ $password }}</code></li>
                    <li class="text-sm font-light flex flex-row md:flex-col"><span class="inline-block w-1/3">Version:</span><code>{{ $serverVersion }}</code></li>
                    <li class="text-sm font-light flex flex-row md:flex-col"><span class="inline-block w-1/3">Day:</span><code>{{ $serverDay }}</code></li>
                </ul>
            </div>
            <!-- end info -->

        </div>
    </div>
    <!-- end update section -->

    <div class="card {{ session()->get('theme', 'light') === 'light' ? '' : 'bg-gray-900 text-gray-100 border-gray-900' }}">
        <div class="card-header {{ session()->get('theme', 'light') === 'light' ? '' : 'border-gray-900' }}">Players <span class="lowercase">({{ $onlinePlayers }} / {{ $totalPlayers }} online)</span></div>

        @foreach($players as $player)
            <div class="hover:opacity-100 duration-200 transition-opacity p-6 flex flex-col text-gray-600 border-b {{ $player->isOffline() ? 'opacity-75 bg-gray-100' : '' }} {{ session()->get('theme', 'light') === 'light' ? '' : ($player->isOffline() ? 'bg-gray-800' : 'bg-gray-700') . ' text-gray-100 border-gray-900' }}">
                <div class="flex flex-row justify-between items-center">
                    <div class="flex flex-col">
                        <h2 class="{{ session()->get('theme', 'light') === 'light' ? '' : 'text-gray-200' }}">{{ $player->lastCharacter() ? $player->lastCharacter()->character_name : 'Unknown' }}</h2>
                        <span class="{{ session()->get('theme', 'light') === 'light' ? '' : 'text-gray-500' }} text-sm">{{ $player->steam_id }}</span>
                    </div>
                    <div class="flex items-center {{ session()->get('theme', 'light') === 'light' ? '' : 'text-gray-200' }}">
                        {{ $player->totalPlayTimeFormatted() }}
                    </div>
                </div>
                <span class="mt-3 text-gray-500 flex flex-row">
                    <span class="mr-4" title="Player Deaths"><i class="fa fa-skull text-gray-400"></i> {{ $player->deaths()->count() }}</span>
                    <span class="mr-4" title="Characters"><i class="fa fa-user text-gray-400"></i> {{ $player->characters()->count() }}</span>
                    <span class="mr-4" title="Sign Ins"><i class="fa fa-sign-in-alt text-gray-400"></i> {{ $player->sessions()->count() }}</span>
                    @if ($player->isOffline())
                        <span class="ml-auto" title="Last Online">Last Online: {{ $player->lastOnline() }}</span>
                    @endif
                </span>
            </div>
        @endforeach
    </div>

    {{--
    <div class="card">
        <div class="card-header">Peak times</div>
        <div id="peak-times-chart"></div>
    </div>
    --}}


    <div class="card shadow-md mb-4 mt-auto {{ session()->get('theme', 'light') === 'light' ? '' : 'bg-gray-900 border-gray-900 text-gray-100' }}">
        <div class="card-body">
            <form action="/" method="POST">
                {{ csrf_field() }}
                <button type="submit" name="toggle-lights" id="toggle-lights" value="1">
                    <i class="fa fa-sun mr-3"></i>
                    Turn {{ session()->get('theme', 'light') === 'light' ? 'off' : 'on' }} the lights
                </button>
            </form>
        </div>
    </div>

    <div class="p-2 pb-4 text-center duration-200 transition-colors text-gray-{{ session()->get('theme', 'light') === 'light' ? '400' : '600' }}">
        <p>Created by <a class="hover:text-gray-{{ session()->get('theme', 'light') === 'light' ? '600' : '400' }}" href="https://gitlab.com/castledaft-public" target="gitlab">Jack Stupple (SurDaft)</a> - Adaptation of Cleopatra Tailwind theme.</p>
    </div>

</div>
