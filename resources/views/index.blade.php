@include('base/start')

<div class="h-screen flex flex-row flex-wrap">

  <div class="flex-1 p-6 mt-16 md:mt-2 mx-auto max-w-2xl" id="root"></div>

</div>

@include('base/end')
