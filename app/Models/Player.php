<?php

namespace App\Models;

use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Player extends Model
{
    use HasFactory;

    /**
     * @return bool
     */
    public function isOnline(): bool
    {
        // not logged in yet (probably shouldnt happen, but jic)
        if ($this->last_login === null) {
            return false;
        }

        // logged in, but not out yet
        if ($this->last_logout === null && $this->last_login !== null) {
            return true;
        }

        // last log out is before login
        return $this->last_logout < $this->last_login;
    }

    public function isOffline(): bool
    {
        return !$this->isOnline();
    }

    /**
     * @return HasMany
     */
    public function characters(): HasMany
    {
        return $this->hasMany(Character::class);
    }

    /**
     * @return Character|null
     */
    public function lastCharacter(): ?Character
    {
        return Character::where([
            'id' => $this->last_character
        ])->first();
    }

    /**
     * @return HasMany
     */
    public function sessions(): HasMany
    {
        return $this->hasMany(PlayerSession::class);
    }

    /**
     * @return int
     */
    public function totalPlayTime(): int
    {
        $sessions = $this->sessions()->get();

        if (count($sessions) === 0) {
            return 0;
        }

        $timeInSeconds = 0;
        $dayAgo = strtotime('yesterday');

        foreach ($sessions as $session) {
            $login = $session->login;
            $logout = $session->logout;

            if (!$logout) {
                if ($login < $dayAgo) {
                    // if outside 24hrs ago, then count it as an hr
                    $logout = $login + (60 * 60);
                } else {
                    // less than 24hrs, count as active time
                    $logout = time();
                }
            }

            if ($login && $logout) {
                $timeInSeconds += $logout - $login;
            }
        }

        return $timeInSeconds;
    }

    public function totalPlayTimeFormatted(): string {
        return CarbonInterval::seconds($this->totalPlayTime())
            ->cascade()
            ->forHumans([
                'minimumUnit' => 'minute',
                'short' => true,
                'parts' => 2
            ]);
    }

    public function lastOnline()
    {
        if ($this->last_logout === null) {
            return "N/A";
        }

        if ($this->last_logout < $this->last_login) {
            return "Online now";
        }

        return CarbonInterval::seconds(time() - $this->last_logout)
            ->cascade()
            ->forHumans([
                'minimumUnit' => 'minute',
                'short' => true,
                'parts' => 2
            ]);
    }

    /**
     * @return HasManyThrough
     */
    public function deaths(): HasManyThrough
    {
        return $this->hasManyThrough(
            Death::class,
            Character::class
        );
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $array = [];

        $array['id'] = $this->attributes['id'];
        $array['isOnline'] = $this->isOnline();
        $array['isOffline'] = $this->isOffline();
        $array['steamId'] = $this->attributes['steam_id'];
        $array['characters'] = $this->characters()->count();
        $array['deaths'] = $this->deaths()->count();
        $array['sessions'] = $this->sessions()->count();
        $array['lastCharacter'] = $this->lastCharacter()->toArray();
        $array['lastOnlineString'] = $this->lastOnline();
        $array['playTimeString'] = $this->totalPlayTimeFormatted();
        $array['currentPlayTimeString'] = $this->currentPlayTimeFormatted();

        return $array;
    }

    private function currentPlayTimeFormatted()
    {
        $session = $this->sessions()
            ->where('logout', '=', null)
            ->orderByDesc('id')
            ->first();

        if (!$session) {
            return null;
        }

        $length = time() - $session->login;

        return CarbonInterval::seconds($length)
            ->cascade()
            ->forHumans([
                'minimumUnit' => 'minute',
                'short' => true,
                'parts' => 2
            ]);
    }
}
