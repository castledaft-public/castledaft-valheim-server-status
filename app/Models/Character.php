<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Character extends Model
{
    use HasFactory;

    /**
     * @return BelongsTo
     */
    public function player(): BelongsTo
    {
        return $this->belongsTo(Player::class);
    }

    /**
     * @return HasMany
     */
    public function deaths(): HasMany
    {
        return $this->hasMany(Death::class);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $array = [];

        $array['characterName'] = $this->attributes['character_name'];

        return $array;
    }
}
