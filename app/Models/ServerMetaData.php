<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServerMetaData extends Model
{
    use HasFactory;

    public const KEY_VERSION = 'version';

    public const KEY_VERSION_CHANGE = 'version_change';

    public const KEY_DAY = 'day';

    public const KEY_DAY_CHANGE = 'day_change';
}
