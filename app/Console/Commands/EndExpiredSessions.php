<?php

namespace App\Console\Commands;

use App\Models\PlayerSession;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputOption;

class EndExpiredSessions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'end-expired-sessions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sessions that last longer than 24hrs will be updated to cap at 1hr.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->addOption('dry-run', 'd', InputOption::VALUE_NONE, 'Don\'t actually save anything');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $playerSessions = PlayerSession::query()
            ->where('logout', '==', null)
            ->where('login', '<', strtotime('-24hrs'));

        // we just end up not committing it, causing a dry run
        if ($this->option('dry-run')) {
            DB::beginTransaction();
        }

        foreach ($playerSessions as $session) {
            $session->logout = $session->login + (60 * 60);
            $session->save();

            $this->output->writeln(sprintf(
                'msg="expired session" from="%s" logout="%s"',
                date('Y-m-d H:i:s', $session->login),
                date('Y-m-d H:i:s', $session->logout)
            ));
        }

        return 0;
    }
}
