const mix = require('laravel-mix');
const options = require('./package.json').options;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(options.paths.src.js + '/app.js', options.paths.dist.js + '/app.js')
    .react()
    .sass(options.paths.src.css + '/app.sass', options.paths.dist.css + '/style.css', {}, [
        require('tailwindcss')(options.config.tailwindjs),
        require('autoprefixer')
    ])
    .copyDirectory(options.paths.src.img, options.paths.dist.img);
