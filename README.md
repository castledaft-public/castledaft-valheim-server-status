# CastleDaft - Valheim Server Status

Display player information retrieved from the Valheim log database.

## Todo:
- Add simple laravel project basis
- Database structure
- API Endpoints
    - Get all players
    - Update player (For adding a nickname / ingame name to steam ID)
    - Recent server stats (pulled from prometheus on behalf of frontend)
- Admin section
    - Interface for adding nickname to players
- Main page
    - Show player list
    - Prioritise online at top
    - Display avg. server metrics pulled from prometheus (via an endpoint)
    - Recent saves
    - Death counts
