<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * table of players
         * unique by steamID store a list of people who have been on the server
         * we don't know which character they played as so last_character may
         * not exist, but last_login is determined by the handshake and should
         * always be present, irrespective of the character
         */
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('steam_id')->unique();
            $table->string('last_character')->nullable();
            $table->integer('last_login')->nullable();
            $table->integer('last_logout')->nullable();
            $table->integer('created_at');
            $table->integer('updated_at')->useCurrentOnUpdate();
        });

        Schema::create('player_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id');
            $table->integer('character_id')->nullable();
            $table->integer('login')->nullable();
            $table->integer('logout')->nullable();
            $table->integer('created_at');
            $table->integer('updated_at')->useCurrentOnUpdate();
        });

        /**
         * table of characters.
         * we initially do not know which character belongs to which steamID
         * and so we store them separately and pair them up via a parent key
         */
        Schema::create('characters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id')->nullable();
            $table->string('character_name')->unique();
            $table->integer('created_at');
            $table->integer('updated_at')->useCurrentOnUpdate();
        });

        /**
         * table of character deaths
         * a character death can be identified in the log by 0:0 after a
         * character name. we would just count the number of rows to see
         * death count and when they died.
         */
        Schema::create('deaths', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('character_id');
            $table->integer('created_at');
            $table->integer('updated_at')->useCurrentOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
        Schema::dropIfExists('player_sessions');
        Schema::dropIfExists('characters');
        Schema::dropIfExists('deaths');
    }
}
