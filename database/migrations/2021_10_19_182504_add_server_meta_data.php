<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddServerMetaData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('server_meta_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key_name', 100)->index();
            $table->string('key_value')->nullable();
            $table->integer('created_at');
            $table->integer('updated_at')->useCurrentOnUpdate();
            $table->create();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('server_meta_data');
    }
}
