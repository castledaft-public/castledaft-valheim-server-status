<?php

use App\Models\Player;
use App\Models\ServerMetaData;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/', function (Request $request) {
    if ($request->post('toggle-lights', false)) {
        $theme = session()->get('theme', 'light');
        if ($theme === 'dark') {
            session()->put('theme', 'light');
        } else {
            session()->put('theme', 'dark');
        }
    }

    return redirect('/');
});

Route::get('/', function () {
    return view('index');
});

Route::get('/api/overview', function () {
    $hostname = 'valheim.daft.host:32456';
    $password = 'Carry!Meat_Waiter!1';

    $serverVersion = ServerMetaData::query()
        ->where('key_name', '=', ServerMetaData::KEY_VERSION)
        ->first()
        ->key_value;

    $serverDay = ServerMetaData::query()
        ->where('key_name', '=', ServerMetaData::KEY_DAY)
        ->first()
        ->key_value;

    return new JsonResponse(compact('hostname', 'password', 'serverVersion', 'serverDay'));
});

Route::get('/api/players', function () {
    $players = Player::all()->sortBy(function ($player) {
        return $player->totalPlayTime();
    }, SORT_NUMERIC, true);

    $onlinePlayers = 0;

    $sortGroups = [
        $onlinePlayersGroupKey = "online" => [],
        $offlinePlayersGroupKey = "offline" => [],
    ];

    foreach ($players as $player) {
        if ($player->isOnline()) {
            $onlinePlayers++;
            $sortGroups[$onlinePlayersGroupKey][] = $player;
        } else {
            $sortGroups[$offlinePlayersGroupKey][] = $player;
        }
    }

    $players = array_merge($sortGroups[$onlinePlayersGroupKey], $sortGroups[$offlinePlayersGroupKey]);

    return new JsonResponse([
        'players' => $players,
        'onlinePlayers' => $onlinePlayers
    ]);
});

